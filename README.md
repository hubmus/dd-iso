# dd-create-live-iso-cli :+: Bashmount

### location scripts


     /usr/local/bin/start-dd-create-live-iso-cli.sh
     /usr/local/bin/dd-create-live-iso
     /usr/share/applications/dd-create-live-iso-cli.desktop
     /usr/local/bin/bashmount
     

 ### Run startup script.
    
 `$ start-dd-create-live-iso-cli.sh`  

  - _opens roxterm exec __dd-create-live-iso-cli__ & set roxterm geometry._

 `$ dd-create-live-iso-cli`

  - _shows list usb devices to choose from. refresh media option._
  
  - _opens --tab with bashmount._

  - _format usb to ext4, if not allready done._

  - _search for ISO files in the downloads dir with whiptail._

  - _possible to safely unmount device, when the job is done, with bashmount._

  - _close or create new iso._


##### DEPS ::: __roxterm, whiptail, hwinfo, lolcat, figlet, (option) nerd-fonts-hack, bashmount.__

Install [Bashmount Github](https://github.com/jamielinux/bashmount)

---

### EASY INSTALL

` $ ./install-all-at-once-linux.sh` - Will make scripts excutable and put them in place. NOT the deps.

---

_Made and tested with Archcraft i3._

##### For roxterm arch you maybe need todo the folowing.

    $ nano /etc/environment or .bashrc
    
    add : 
    
    export NO_AT_BRIDGE=1
     
##### Logout/in to take effect.

_Tested on Parrot home xfce4 :+:_

_Tested on Mxlinux-Fluxbox 64/32bit :+:_

---

#### example arcolinuxL xfce4 ...

_start video = adding devices to virtualbox. looks like nothing is happening._

![dd-iso-test-arco-xfce-WHIPTAIL](/uploads/7690207eac34f791ac7529d222bdf952/04-dd-iso-test-arco-xfce-WHIPTAIL-2022-01-26_19.10.56.mp4)

![ArcoLinux_2022-02-20_20-28-01](/uploads/bacea0d90221f2dbec215346230099c6/ArcoLinux_2022-02-20_20-28-01.png) ![ArcoLinux_2022-02-20_20-30-22](/uploads/3dd3e45109087caede9b6f0e1ec01272/ArcoLinux_2022-02-20_20-30-22.png)
